package fi.utu.ville.exercises.mathpathexer.stub;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.server.VaadinRequest;

import fi.utu.ville.exercises.stub.VilleExerStubUI;

@Theme("mathpathexer-stub-theme")
@Title("mathpathexer - Ville-exercise testing-stub")
@SuppressWarnings("serial")
public class StubUI extends VilleExerStubUI
{

    @Override
    public void init(VaadinRequest req) {
        super.init(req);
        System.out.println("base dir:" + getStubResourceBaseDir());
    }
}
