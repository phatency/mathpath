package fi.utu.ville.exercises.mathpathexer;

import java.util.ArrayList;

import com.vaadin.ui.Layout;
import com.vaadin.ui.Notification;
import com.vaadin.ui.VerticalLayout;

import fi.utu.ville.exercises.helpers.ExerciseExecutionHelper;
import fi.utu.ville.exercises.model.ExecutionSettings;
import fi.utu.ville.exercises.model.ExecutionState;
import fi.utu.ville.exercises.model.ExecutionStateChangeListener;
import fi.utu.ville.exercises.model.Executor;
import fi.utu.ville.exercises.model.ExerciseException;
import fi.utu.ville.exercises.model.SubmissionListener;
import fi.utu.ville.exercises.model.SubmissionType;
import fi.utu.ville.standardutils.Localizer;
import fi.utu.ville.standardutils.TempFilesManager;

public class MathPathExerExecutor extends VerticalLayout implements
		Executor<MathPathExerExerciseData, MathPathExerSubmissionInfo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2682119786422750060L;

	private final ExerciseExecutionHelper<MathPathExerSubmissionInfo> execHelper =

	new ExerciseExecutionHelper<MathPathExerSubmissionInfo>();
	
	private MathPathExerViewConstructor viewConstructor;

	public MathPathExerExecutor() {

	}

	@Override
	public void initialize(Localizer localizer,
			MathPathExerExerciseData exerciseData, MathPathExerSubmissionInfo oldSubm,
			TempFilesManager materials, ExecutionSettings fbSettings)
			throws ExerciseException {
	    
	    viewConstructor = new MathPathExerViewConstructor(localizer, exerciseData, oldSubm);
	    this.addComponent(viewConstructor);
	}

	@Override
	public void registerSubmitListener(
			SubmissionListener<MathPathExerSubmissionInfo> submitListener) {
		execHelper.registerSubmitListener(submitListener);
	}

	@Override
	public Layout getView() {
		return this;
	}

	@Override
	public void shutdown() {
		// nothing to do here
	}

	@Override
	public void askReset() {
		viewConstructor.reset();
		System.out.println("RESET");
	}

	@Override
	public ExecutionState getCurrentExecutionState() {
		return execHelper.getState();
	}

	@Override
	public void askSubmit(SubmissionType submType) {
	    if (viewConstructor.pathReachesGoal()) {
	        ArrayList<Integer> path = viewConstructor.getPath();
	        
	        double success = viewConstructor.checkSuccess();
	        execHelper.informOnlySubmit(success, new MathPathExerSubmissionInfo(
	                 viewConstructor.getSeed(), path), submType, null);
	     } else {
	        Notification.show("Path doesn't reach goal.");
	     }
	}

	@Override
	public void registerExecutionStateChangeListener(
			ExecutionStateChangeListener execStateListener) {
		execHelper.registerExerciseExecutionStateListener(execStateListener);

	}

}
