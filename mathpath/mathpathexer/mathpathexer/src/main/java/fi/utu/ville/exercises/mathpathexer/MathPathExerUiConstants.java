package fi.utu.ville.exercises.mathpathexer;

public class MathPathExerUiConstants {

	private static final String PREFIX = "MATHPATHEXER" + ".";

	public static final String NAME = PREFIX + "NAME";

	public static final String DESC = PREFIX + "DESC";

	public static final String QUESTION = PREFIX + "QUESTION";
	
	public static final String ANSWER = PREFIX + "ANSWER";
	
	public static final String SUBM_EXPORT = PREFIX + "SUBM_EXPORT";
	
	public static final String ANSWER_COL_DESC = PREFIX + "ANSWER_COL_DESC";
	
	public static final String MINIMUM_VALUE = PREFIX + "MINIMUM_VALUE";
	
	public static final String MAXIMUM_VALUE = PREFIX + "MAXIMUM_VALUE";
	
	public static final String DIFFICULTY = PREFIX + "DIFFICULTY";
	
	public static final String DIFFICULTY_TOOLTIP = PREFIX + "DIFFICULTY_TOOLTIP";
	
	public static final String OPERATORS = PREFIX + "OPERATORS";
	
}
