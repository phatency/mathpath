package fi.utu.ville.exercises.mathpathexer;

import com.vaadin.server.Resource;

import fi.utu.ville.exercises.helpers.GsonPersistenceHandler;
import fi.utu.ville.exercises.model.ExerciseTypeDescriptor;
import fi.utu.ville.exercises.model.PersistenceHandler;
import fi.utu.ville.exercises.model.SubmissionStatisticsGiver;
import fi.utu.ville.exercises.model.SubmissionVisualizer;
import fi.utu.ville.standardutils.Localizer;

public class MathPathExerDescriptor implements
		ExerciseTypeDescriptor<MathPathExerExerciseData, MathPathExerSubmissionInfo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5743225101617556960L;

	public static final MathPathExerDescriptor INSTANCE = new MathPathExerDescriptor();

	private MathPathExerDescriptor() {

	}

	@Override
	public PersistenceHandler<MathPathExerExerciseData, MathPathExerSubmissionInfo> newExerciseXML() {
		// You can also implement your own PersistenceHandler if you want (see JavaDoc for more info)
		return new GsonPersistenceHandler<MathPathExerExerciseData, MathPathExerSubmissionInfo>(
				getTypeDataClass(), getSubDataClass());
	}

	@Override
	public MathPathExerExecutor newExerciseExecutor() {
		return new MathPathExerExecutor();
	}

	@Override
	public MathPathExerEditor newExerciseEditor() {
		return new MathPathExerEditor();
	}

	@Override
	public Class<MathPathExerExerciseData> getTypeDataClass() {
		return MathPathExerExerciseData.class;
	}

	@Override
	public Class<MathPathExerSubmissionInfo> getSubDataClass() {
		return MathPathExerSubmissionInfo.class;
	}

	@Override
	public SubmissionStatisticsGiver<MathPathExerExerciseData, MathPathExerSubmissionInfo> newStatisticsGiver() {
		return new MathPathExerStatisticsGiver();
	}

	@Override
	public SubmissionVisualizer<MathPathExerExerciseData, MathPathExerSubmissionInfo> newSubmissionVisualizer() {
		return new MathPathExerSubmissionViewer();
	}

	@Override
	public String getTypeName(Localizer localizer) {
		return localizer.getUIText(MathPathExerUiConstants.NAME);
	}

	@Override
	public String getTypeDescription(Localizer localizer) {
		return localizer.getUIText(MathPathExerUiConstants.DESC);
	}

	@Override
	public Resource getSmallTypeIcon() {
		return MathPathExerIcon.SMALL_TYPE_ICON.getIcon();
	}

	@Override
	public Resource getMediumTypeIcon() {
		return MathPathExerIcon.SMALL_TYPE_ICON.getIcon();
	}

	@Override
	public Resource getLargeTypeIcon() {
		return MathPathExerIcon.SMALL_TYPE_ICON.getIcon();
	}
	
	@Override
	public boolean isManuallyGraded() {
		return false;
	}

}