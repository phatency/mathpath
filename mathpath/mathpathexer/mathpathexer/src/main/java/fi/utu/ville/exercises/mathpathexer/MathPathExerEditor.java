package fi.utu.ville.exercises.mathpathexer;

import java.util.ArrayList;
import java.util.EnumSet;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import fi.utu.ville.exercises.mathpathexer.generator.ExpressionGeneratorParameters;
import fi.utu.ville.exercises.mathpathexer.generator.GeneratorHelper;
import fi.utu.ville.exercises.mathpathexer.generator.MathOperator;
import fi.utu.ville.exercises.model.Editor;
import fi.utu.ville.exercises.model.EditorHelper;
import fi.utu.ville.exercises.model.EditorHelper.EditedExerciseGiver;
import fi.utu.ville.standardutils.AbstractFile;
import fi.utu.ville.standardutils.Localizer;

public class MathPathExerEditor extends HorizontalLayout implements
		Editor<MathPathExerExerciseData> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4600841604409240872L;
	
	private EditorHelper<MathPathExerExerciseData> editorHelper;
	
	private TextField minValueText;
	
	private TextField maxValueText;
	
	private NativeSelect depthSelect;
	
	private TextField widthText;
	
	private TextField heightText;
	
	private ArrayList<CheckBox> operatorCheckBoxes;

	private AbstractFile currImgFile;
	
	private ExpressionGeneratorParameters generatorParameters;

	private Localizer localizer;

	public MathPathExerEditor() {
	    
	}

	@Override
	public Layout getView() {
		return this;
	}

	@Override
	public void initialize(Localizer localizer, MathPathExerExerciseData oldData,
			EditorHelper<MathPathExerExerciseData> editorHelper) {

		this.localizer = localizer;

		this.editorHelper = editorHelper;

		editorHelper.getTempManager().initialize();

		doLayout(oldData);
	}

	private MathPathExerExerciseData getCurrentExercise() {
	        updateGeneratorParameters();
		return new MathPathExerExerciseData(generatorParameters, getWidthValue(), getHeightValue());
	}

	private void doLayout(MathPathExerExerciseData oldData) {

		this.setMargin(true);
		this.setSpacing(true);
		this.setWidth("100%");

		int oldWidth=4, oldHeight=6;
		if (oldData != null) {
			generatorParameters = oldData.getGeneratorParameters();
			if(generatorParameters == null) {
			    generatorParameters = new ExpressionGeneratorParameters();
			}
			
			oldWidth = oldData.getWidth();
			oldHeight = oldData.getHeight();
		} else {
			currImgFile = null;
			generatorParameters = new ExpressionGeneratorParameters();
		}

		VerticalLayout controlsLayout = new VerticalLayout();
		controlsLayout.setWidth("400px");

		controlsLayout.addComponent(editorHelper.getInfoEditorView());
		
		controlsLayout.addComponent(editorHelper
				.getControlbar(new EditedExerciseGiver<MathPathExerExerciseData>() {

					@Override
					public MathPathExerExerciseData getCurrExerData(
							boolean forSaving) {
						return getCurrentExercise();
					}
				}));
		this.addComponent(controlsLayout);

		VerticalLayout editLayout = new VerticalLayout();
		
//		Label questionTextCapt = new Label(
//				localizer.getUIText(MathPathExerUiConstants.QUESTION));
//		questionTextCapt.addStyleName(MathPathExerThemeConsts.TITLE_STYLE);
//		questionText = new TextField(null, oldQuestion);
		
		Label minValueCapt = new Label(
//		        localizer.getUIText(MathPathExerUiConstants.MINIMUM_VALUE));
		        "Minimum value");
		minValueCapt.addStyleName(MathPathExerThemeConsts.TITLE_STYLE);
		minValueText = new TextField(null, "" + generatorParameters.getMinValue());
		minValueText.setConverter(Integer.class);
		
	        Label maxValueCapt = new Label(
//	                localizer.getUIText(MathPathExerUiConstants.MAXIMUM_VALUE));
	                "Maximum value");
	        
                maxValueCapt.addStyleName(MathPathExerThemeConsts.TITLE_STYLE);
                maxValueText = new TextField(null, "" + generatorParameters.getMaxValue());
                maxValueText.setConverter(Integer.class);
                
                Label widthCapt = new Label("Width");
                widthCapt.addStyleName(MathPathExerThemeConsts.TITLE_STYLE);
                widthText = new TextField(null, "" + oldWidth);
                widthText.setConverter(Integer.class);
                
                Label heightCapt = new Label("Height");
                heightCapt.addStyleName(MathPathExerThemeConsts.TITLE_STYLE);
                heightText = new TextField(null, "" + oldHeight);
                heightText.setConverter(Integer.class); 
                
                Label operatorCapt = new Label(
//                        localizer.getUIText(MathPathExerUiConstants.OPERATORS));
                        "Operators");
                
                operatorCheckBoxes = new ArrayList<CheckBox>(); 
                for(MathOperator operator : MathOperator.ALL_OPERATORS) {
                    CheckBox cb = new CheckBox(operator.getName());
                    if(GeneratorHelper.existsInCollection(generatorParameters.getOperators(), operator)) {
                        cb.setValue(true);
                    }
                    cb.setData(operator);
                    cb.addValueChangeListener(new ValueChangeListener() {
                        
                        @Override
                        public void valueChange(ValueChangeEvent event) {
                            updateGeneratorParameters();
                        }
                    });
                    operatorCheckBoxes.add(cb);
                }
                
                depthSelect = new NativeSelect("Difficulty");
                // localizer.getUIText(MathPathExerUiConstants.DIFFICULTY_TOOLTIP)
                for(int i = 1; i <= 3; i++) {
                    depthSelect.addItem(i);
                    depthSelect.setItemCaption(i, "" + (i));
                }
                depthSelect.setNullSelectionAllowed(false);
                depthSelect.setValue(generatorParameters.getMaxDepth()-1);
                
                editLayout.addComponent(minValueCapt);
                editLayout.addComponent(minValueText);
                
                editLayout.addComponent(maxValueCapt);
                editLayout.addComponent(maxValueText);
                
                editLayout.addComponent(operatorCapt);
                for(CheckBox operatorCb : operatorCheckBoxes) {
                    editLayout.addComponent(operatorCb);
                }
                
                editLayout.addComponent(depthSelect);
                
                
                editLayout.addComponent(widthCapt);
                editLayout.addComponent(widthText);
                
                editLayout.addComponent(heightCapt);
                editLayout.addComponent(heightText);
		this.addComponent(editLayout);
	}
	
	private void updateGeneratorParameters() {
	    EnumSet<MathOperator> checkedOperators = EnumSet.noneOf(MathOperator.class);
	    for(CheckBox cb : operatorCheckBoxes) {
	        if(cb.getValue()) {
	            checkedOperators.add((MathOperator)cb.getData());
	        }
	    }
	    generatorParameters.setOperators(checkedOperators);
	    
	    generatorParameters.setMinValue(Integer.parseInt(minValueText.getValue()));
	    generatorParameters.setMaxValue(Integer.parseInt(maxValueText.getValue()));
	    generatorParameters.setMaxDepth((Integer)depthSelect.getValue()+1);

	    
	}
	
	private int getWidthValue() {
	    return Integer.parseInt(widthText.getValue());
	}
	
	private int getHeightValue() {
	    return Integer.parseInt(heightText.getValue());
	}
}
