package fi.utu.ville.exercises.mathpathexer;

import java.util.ArrayList;

import fi.utu.ville.exercises.model.SubmissionInfo;

public class MathPathExerSubmissionInfo implements SubmissionInfo {

    private static final long serialVersionUID = 8702870727095225372L;

    private final long seed;
    private final ArrayList<Integer> path;

    public MathPathExerSubmissionInfo(long seed, ArrayList<Integer> path) {
        this.seed = seed;
        this.path = path;
    }

    public long getSeed() {
        return seed;
    }

    public ArrayList<Integer> getPath() {
        return path;
    }
    
    public String getAnswer() {
        return "";
    }
}
