package fi.utu.ville.exercises.mathpathexer;

import java.util.ArrayList;
import java.util.Random;

import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.AbstractComponent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import fi.utu.ville.exercises.helpers.ExerciseExecutionHelper;
import fi.utu.ville.exercises.mathpathexer.generator.ExpressionGenerator;
import fi.utu.ville.standardutils.Localizer;

public class MathPathExerViewConstructor extends VerticalLayout {

        /**
         * 
         */
        private static final long serialVersionUID = 2682119786422750060L;

        private final ExerciseExecutionHelper<MathPathExerSubmissionInfo> execHelper =

        new ExerciseExecutionHelper<MathPathExerSubmissionInfo>();
        
        private ExpressionGenerator generator;

        private final TextField answerField = new TextField();


            private int width, height;
            
            private MathPathBlock[][] mathPathBlocks;
            
            private MathPathBlock firstBlockInPath, startBlock, goalBlock;

            private Random random;
            private long seed;

            public MathPathExerViewConstructor(Localizer localizer, MathPathExerExerciseData exerciseData, MathPathExerSubmissionInfo oldSubm) {
                
                firstBlockInPath = null;
                random = new Random();
                if(oldSubm == null) {
                    seed = random.nextLong();
                } else {
                    seed = oldSubm.getSeed();
                }
                random.setSeed(seed);
                
                answerField.setCaption(
                        localizer.getUIText(MathPathExerUiConstants.ANSWER));
                this.width = exerciseData.getWidth();
                this.height = exerciseData.getHeight();
                mathPathBlocks = new MathPathBlock[height][width];
                generator = new ExpressionGenerator(exerciseData.getGeneratorParameters());
                generateLayout(exerciseData);
                generatePath();
                fillWithDummyData();
                if(oldSubm != null) {
                    ArrayList<Integer> oldPath = oldSubm.getPath();
                    for(Integer val : oldPath) {
                        toggleBlock((val / width),val % width);
                    }
                }
            }

        public void generateLayout(MathPathExerExerciseData exerciseData) {
                GridLayout mathPathGridLayout = new GridLayout(width, height);
                mathPathGridLayout.setStyleName("xcenter");
                this.addComponent(mathPathGridLayout);

                boolean startSet = false, goalSet = false;
                for (int i = 0; i < height; i++) {
                    for (int j = 0; j < width; j++) {
                        final AbstractComponent button;
                        final int iii = i;
                        final int jjj = j;
                        if (!goalSet
                                && (j == width - 1 || width * random.nextDouble() <= 1.0)) {
                            button = new Label("Goal");
                            goalBlock = new MathPathBlock(button,
                                    MathPathBlock.Status.goal, jjj, iii);
                            button.addStyleName("not-pushable");
                            button.addStyleName("unchecked");
                            mathPathBlocks[i][j] = goalBlock;
                            goalSet = true;
                        } else if ((i == height - 1 && !startSet)
                                && (j == width - 1 || width * random.nextDouble() <= 1.0)) {
                            button = new Label("<div height=\"10em\" class=\"vertical-center\">" + generator.getValue() + "</div>");
                            ((Label)button).setContentMode(ContentMode.HTML);
                            startBlock = new MathPathBlock(button,
                                    MathPathBlock.Status.start, jjj, iii);
                            button.addStyleName("not-pushable");
                            button.addStyleName("checked");
                            mathPathBlocks[i][j] = startBlock;
                            
                            startSet = true;
                        } else {
                            button = new Button();
                            mathPathBlocks[i][j] = new MathPathBlock(button, jjj, iii);

                            ((Button) button).addClickListener(new ClickListener() {
                                public void buttonClick(ClickEvent event) {
                                    toggleBlock(iii, jjj);
                                }
                            });
                        }
                        mathPathGridLayout.addComponent(button);
                    }
                }
        }
        
        private void generatePath() {
            int x = startBlock.getX(), y = height - 1, dx = goalBlock.getX();
            while ((y != 1 || x != dx) && (y > 0 || Math.abs(x - dx) > 1)) {
                if (y == 0) {
                    if (x < dx - 1) {
                        x++;
                    }
                    if (x > dx + 1) {
                        x--;
                    }
                } else {
                    int code = random.nextInt(3);
                    switch (code) {
                    case 0:
                        y--;
                        break;
                    case 1:
                        if (x > 0 && !mathPathBlocks[y][x - 1].isInitialized()) {
                            x--;
                        }
                        break;
                    case 2:
                        if (x < width - 1
                                && !mathPathBlocks[y][x + 1].isInitialized()) {
                            x++;
                        }
                        break;
                    }
                }
                mathPathBlocks[y][x].initialize(generator.generateRightExpression().toString() + "!", true);
            }
        }

        private void fillWithDummyData() {
            for (int i = 0; i < height; i++) {
                for (int j = 0; j < width; j++) {
                    if (!mathPathBlocks[i][j].isInitialized()) {
                        mathPathBlocks[i][j].initialize(generator.generateWrongExpression().toString(), false);
                    }
                }
            }
        }

            public void toggleBlock(int y, int x) {
                MathPathBlock mpb = mathPathBlocks[y][x];
                mpb.setCheckedness(!mpb.isChecked());
                if (mpb.isChecked()) {
                    if (firstBlockInPath == null) {
                        int x1 = mpb.getX(), y1 = mpb.getY(), x2 = startBlock.getX(), y2 = startBlock
                                .getY();
                        if ((x1 == x2 && Math.abs(y1 - y2) <= 1)
                                || (y1 == y2 && Math.abs(x1 - x2) <= 1)) {
                            firstBlockInPath = mpb;
                        } else {
                            Notification
                                    .show("First block must be placed next to start!");
                            mpb.setCheckedness(!mpb.isChecked());
                            firstBlockInPath = null;
                        }
                    } else {
                        MathPathBlock bip = firstBlockInPath;
                        while (bip.getNext() != null) {
                            bip = bip.getNext();
                        }
                        int x1 = mpb.getX(), y1 = mpb.getY(), x2 = bip.getX(), y2 = bip
                                .getY();
                        if ((x1 == x2 && Math.abs(y1 - y2) <= 1)
                                || (y1 == y2 && Math.abs(x1 - x2) <= 1)) {
                            bip.setNext(mpb);
                        } else {
                            Notification
                                    .show("Can only place blocks next to the head!");
                            mpb.setCheckedness(!mpb.isChecked());
                        }

                    }
                } else {
                    MathPathBlock bip = firstBlockInPath;
                    if (bip == mpb) {
                        if (bip.getNext() == null) {
                            firstBlockInPath = null;
                        } else {
                            Notification.show("Can only remove blocks at the head.");
                            mpb.setCheckedness(!mpb.isChecked());
                        }
                    } else {
                        while (bip.getNext() != mpb) {
                            bip = bip.getNext();
                        }
                        if (bip.getNext().getNext() == null) {
                            bip.setNext(null);
                        } else {
                            Notification.show("Can only remove blocks at the head.");
                            mpb.setCheckedness(!mpb.isChecked());
                        }
                    }
                }
            }
            
            public boolean pathReachesGoal() {
                if (firstBlockInPath == null) {
                    return false;
                }
                MathPathBlock bip = firstBlockInPath;
               // while (bip.getNext() != null) {
                for(int i=0; i< 1000 && bip.getNext() != null; i++) {
                    bip = bip.getNext();
                    if(i ==999) {
                        Notification.show("over 9000");
                    }
                }
                
                int x1 = bip.getX(), y1 = bip.getY(), x2 = goalBlock.getX(), y2 = goalBlock
                        .getY();
                if ((x1 == x2 && Math.abs(y1 - y2) <= 1)
                        || (y1 == y2 && Math.abs(x1 - x2) <= 1)) {
                    return true;
                }
                return false;
            }

            public ArrayList<Integer> getPath() {
                ArrayList<Integer> blockList = new ArrayList<Integer>();
                MathPathBlock bip = firstBlockInPath;
                while (bip != null) {
                    blockList.add(width * bip.getY() + bip.getX());
                    bip = bip.getNext();
                }
                return blockList;
            }

            public double checkSuccess() {
                int blocksTraversed = 0, blocksCorrect = 0;
                MathPathBlock bip = firstBlockInPath;
                while (bip != null) {
                    blocksTraversed++;
                    if (bip.isCorrect()) {
                        blocksCorrect++;
                    }
                    bip = bip.getNext();
                }
                return (1.0 * blocksCorrect / blocksTraversed) ;
            }
            
            public long getSeed() {
                return seed;
            }

            public void reset() {
                ArrayList<MathPathBlock> path = new ArrayList<MathPathBlock>();
                MathPathBlock bip = firstBlockInPath;
                while(bip != null) {
                    path.add(bip);
//                    toggleBlock(bip.getY(), bip.getX());
                    bip = bip.getNext();
                }
                
                for(int i = path.size()-1; i  >=0; i--) {
                    bip = path.get(i);
                    toggleBlock(bip.getY(), bip.getX());
                }
                
            }
            
}
