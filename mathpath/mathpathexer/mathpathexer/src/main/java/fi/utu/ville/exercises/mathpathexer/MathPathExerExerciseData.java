package fi.utu.ville.exercises.mathpathexer;

import fi.utu.ville.exercises.mathpathexer.generator.ExpressionGeneratorParameters;
import fi.utu.ville.exercises.model.ExerciseData;

public class MathPathExerExerciseData implements ExerciseData {

    /**
	 * 
	 */
    private static final long serialVersionUID = -716445297446246493L;
    
    private final ExpressionGeneratorParameters parameters;
    
    private final int width;
    
    private final int height;

    public MathPathExerExerciseData(ExpressionGeneratorParameters parameters, int width, int height) {
        this.parameters = parameters;
        this.width = width;
        this.height = height;
    }

    public String getQuestion() { return "";}
    
    public int getWidth() {
        return width;
    }
    
    public int getHeight() {
        return height;
    }
    
    public ExpressionGeneratorParameters getGeneratorParameters() {
        return parameters;
    }

}
