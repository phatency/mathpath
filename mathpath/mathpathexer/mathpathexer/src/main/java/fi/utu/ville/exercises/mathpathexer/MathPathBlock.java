package fi.utu.ville.exercises.mathpathexer;

import com.vaadin.ui.AbstractComponent;

public class MathPathBlock {
    private AbstractComponent widget;
    private MathPathBlock next = null;
    private Status status;
    private boolean isCorrect = false;
    private int x, y;

    public enum Status {
        uninitialized, unchecked, checked, start, goal;
    }

    public MathPathBlock(AbstractComponent widget, Status status, int x, int y) {
        this.widget = widget;
        this.status = status;
        this.x = x;
        this.y = y;
    }

    public MathPathBlock(AbstractComponent widget, int x, int y) {
        this(widget, Status.uninitialized, x, y);
    }

    public MathPathBlock getNext() {
        return next;
    }

    public void setNext(MathPathBlock next) {
        this.next = next;
    }

    public boolean isChecked() {
        return status == Status.checked;
    }

    public void setCheckedness(boolean checked) {
        status = checked == true ? Status.checked : Status.unchecked;
        
        if (status == Status.checked) {
            widget.setStyleName("checked");
            widget.removeStyleName("unchecked");
        } else {
            widget.setStyleName("unchecked");
            widget.removeStyleName("checked");
        }
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void initialize(String text, boolean isCorrect) {
        if (status == Status.uninitialized) {
            this.isCorrect = isCorrect;
            widget.setCaption(text);
            widget.setStyleName("unchecked");
            status = Status.unchecked;
            
        }
    }

    public boolean isInitialized() {
        return status != Status.uninitialized;
    }

    public boolean isCorrect() {
        return isCorrect;
    }
}
