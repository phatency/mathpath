package fi.utu.ville.exercises.mathpathexer;

public class MathPathExerThemeConsts {
	
	public static final String ANSWER_STYLE = "MathPathExer-answer";
	
	public static final String TITLE_STYLE = "MathPathExer-title";
	
}