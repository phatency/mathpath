package fi.utu.ville.exercises.mathpathexer;

import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import fi.utu.ville.exercises.model.ExerciseException;
import fi.utu.ville.exercises.model.SubmissionVisualizer;
import fi.utu.ville.standardutils.Localizer;
import fi.utu.ville.standardutils.TempFilesManager;

public class MathPathExerSubmissionViewer extends VerticalLayout implements
		SubmissionVisualizer<MathPathExerExerciseData, MathPathExerSubmissionInfo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6260031633710031462L;
	private MathPathExerExerciseData exer;
	private MathPathExerSubmissionInfo submInfo;

	private Localizer localizer;
	
	public MathPathExerSubmissionViewer() {
	}

	@Override
	public void initialize(MathPathExerExerciseData exerciseData,
			MathPathExerSubmissionInfo submInfo, Localizer localizer,
			TempFilesManager tempManager) throws ExerciseException {
		this.localizer = localizer;
		this.exer = exerciseData;
		this.submInfo = submInfo;
		this.addComponent(new MathPathExerViewConstructor(localizer, exerciseData, submInfo));
		doLayout();
	}

	private void doLayout() {
		this.addComponent(new Label(localizer.getUIText(MathPathExerUiConstants.QUESTION) + 
				": " + exer.getQuestion()));
		Label answ = new Label(localizer.getUIText(MathPathExerUiConstants.ANSWER) + 
				": "  + submInfo.getAnswer());
		answ.addStyleName(MathPathExerThemeConsts.ANSWER_STYLE);
		this.addComponent(answ);
	}

	@Override
	public Component getView() {
		return this;
	}

	@Override
	public String exportSubmissionDataAsText() {
		return localizer.getUIText(MathPathExerUiConstants.QUESTION, "\n", 
				exer.getQuestion(), submInfo.getAnswer());
		
	}

}
