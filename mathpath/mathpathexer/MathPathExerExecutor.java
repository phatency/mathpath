package fi.utu.ville.exercises.mathpathexer;

import java.awt.Point;

import com.vaadin.event.LayoutEvents;
import com.vaadin.event.LayoutEvents.LayoutClickEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Component;
import com.vaadin.ui.GridLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;

import fi.utu.ville.exercises.helpers.ExerciseExecutionHelper;
import fi.utu.ville.exercises.model.ExecutionSettings;
import fi.utu.ville.exercises.model.ExecutionState;
import fi.utu.ville.exercises.model.ExecutionStateChangeListener;
import fi.utu.ville.exercises.model.Executor;
import fi.utu.ville.exercises.model.ExerciseException;
import fi.utu.ville.exercises.model.SubmissionListener;
import fi.utu.ville.exercises.model.SubmissionType;
import fi.utu.ville.standardutils.Localizer;
import fi.utu.ville.standardutils.TempFilesManager;

public class MathPathExerExecutor extends VerticalLayout implements
		Executor<MathPathExerExerciseData, MathPathExerSubmissionInfo> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2682119786422750060L;

	private final ExerciseExecutionHelper<MathPathExerSubmissionInfo> execHelper =

	new ExerciseExecutionHelper<MathPathExerSubmissionInfo>();

	private final TextField answerField = new TextField();

	public MathPathExerExecutor() {

	}

	@Override
	public void initialize(Localizer localizer,
			MathPathExerExerciseData exerciseData, MathPathExerSubmissionInfo oldSubm,
			TempFilesManager materials, ExecutionSettings fbSettings)
			throws ExerciseException {
		answerField.setCaption(
		        localizer.getUIText(MathPathExerUiConstants.ANSWER));

		doLayout(exerciseData, oldSubm != null ? oldSubm.getAnswer() : "");
		
	}

	private void doLayout(MathPathExerExerciseData exerciseData, String oldAnswer) {

		GridLayout grid = new GridLayout(4,4);
		grid.setStyleName("mathpathexer-mathgrid");
		final Button testButton = new Button("(1+2)*(6/2)");
		this.addComponent(testButton);
		testButton.setStyleName("mathpathexer-expression");
		testButton.addClickListener(new ClickListener() {
                    @Override
                    public void buttonClick(ClickEvent event) {
                        if(testButton.getData() == null) {
                           testButton.setStyleName("mathpathexer-expression-selected");
                           testButton.removeStyleName("mathpathexer-expression");
                           testButton.setData(0);
                        } else {
                            testButton.setStyleName("mathpathexer-expression");
                            testButton.removeStyleName("mathpathexer-expression-selected");
                            testButton.setData(null);
                        }
                    }
                });
		grid.addLayoutClickListener(new LayoutEvents.LayoutClickListener() {
                    
                    @Override
                    public void layoutClick(LayoutClickEvent event) {
                        answerField.setValue("Click");
                        Component clickedComponent = event.getChildComponent();
                        if (clickedComponent instanceof Label) {
                            Label label = (Label) clickedComponent;  
                            Point p = (Point)label.getData();
                            answerField.setValue(p.toString());
                            label.setStyleName("mathpathexer-expression-click");
                        }
                    }
                });
		
		for(int i = 0; i < 4; i++)
		    for(int j = 0; j < 4; j++) {
		        Label label = new Label("" + i + "," + j);
		        label.setData(new Point(i,j));
		        label.setStyleName("mathpathexer-expression");
		        grid.addComponent(label,i,j);
		    }
		this.addComponent(grid);
		answerField.setValue(oldAnswer);
		
		this.addComponent(answerField);
	}

	@Override
	public void registerSubmitListener(
			SubmissionListener<MathPathExerSubmissionInfo> submitListener) {
		execHelper.registerSubmitListener(submitListener);
	}

	@Override
	public Layout getView() {
		return this;
	}

	@Override
	public void shutdown() {
		// nothing to do here
	}

	@Override
	public void askReset() {
		// nothing to do here
	}

	@Override
	public ExecutionState getCurrentExecutionState() {
		return execHelper.getState();
	}

	@Override
	public void askSubmit(SubmissionType submType) {
		double corr = 1.0;

		String answer = answerField.getValue();
		execHelper.informOnlySubmit(corr, new MathPathExerSubmissionInfo(answer),
				submType, null);

	}

	@Override
	public void registerExecutionStateChangeListener(
			ExecutionStateChangeListener execStateListener) {
		execHelper.registerExerciseExecutionStateListener(execStateListener);

	}

}
