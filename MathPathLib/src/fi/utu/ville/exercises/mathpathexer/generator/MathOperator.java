package fi.utu.ville.exercises.mathpathexer.generator;

import java.util.EnumSet;

public enum MathOperator {
    MULTIPLICATION(Arity.BINARY, '*', 3), DIVISION(Arity.BINARY, '/', 3), PLUS(
            Arity.BINARY, '+', 2), MINUS(Arity.BINARY, '-', 2), VALUE(
            Arity.NULLARY, '\0', 0), NOOP(Arity.NULLARY, '?', 0);
    public static final EnumSet<MathOperator> BINARY_OPERATORS = EnumSet.range(MULTIPLICATION, MINUS);
    public static final EnumSet<MathOperator> ALL_OPERATORS = EnumSet.range(MULTIPLICATION, MINUS);
    private Arity arity;
    private char representation;
    private int precedence;

    MathOperator(Arity arity, char representation, int precedence) {
        this.arity = arity;
        this.representation = representation;
        this.precedence = precedence;
    }

    public int getPredecence() {
        return precedence;
    }

    public Arity getArity() {
        return this.arity;
    }

    public char getOperatorChar() {
        return representation;
    }
    
    public String getName() {
        return this.toString().toLowerCase();
    }
    
    public boolean hasValue() {
        return (this == VALUE);
    }

    public Integer Evaluate(Integer left, Integer right) {
        switch (this) {
        case MULTIPLICATION:
            return left * right;
        case DIVISION:
            if(right != 0) {
                return left / right;
            }
            return Integer.MIN_VALUE;
        case PLUS:
            return left + right;
        case MINUS:
            return left - right;
        case VALUE:
            return left;
        }
        return 0;
    }
}
