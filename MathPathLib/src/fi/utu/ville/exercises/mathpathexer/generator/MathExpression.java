package fi.utu.ville.exercises.mathpathexer.generator;

enum Arity {
    NULLARY, UNARY, BINARY
}

public class MathExpression {
    private MathOperator operator;
    private MathExpression left;
    private MathExpression right;
    private MathExpression parent = null;
    private Integer value;

    public MathExpression(MathOperator operator, MathExpression left,
            MathExpression right, Integer value) {
        this.operator = operator;
        this.left = left;
        this.right = right;
        this.value = value;
    }

    public MathExpression(MathOperator operator, MathExpression left,
            MathExpression right) {
        this(operator, left, right, null);
    }

    public MathExpression(MathOperator operator, MathExpression left) {
        this(operator, left, null, null);
    }

    public MathExpression(MathOperator operator, Integer value) {
        this(operator, null, null, value);
    }

    public MathExpression() {
        this.operator = MathOperator.NOOP;

    }

    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    public void setOperator(MathOperator operator) {
        this.operator = operator;
    }

    private void setParent(MathExpression parent) {
        this.parent = parent;
    }

    public void setLeft(MathExpression left) {
        this.left = left;
        left.setParent(this);
    }

    public void setRight(MathExpression right) {
        this.right = right;
        right.setParent(this);
    }

    public MathExpression getLeft() {
        return left;
    }

    public MathExpression getRight() {
        return right;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public void setExpression(MathOperator operator, Integer left, Integer right) {
        this.operator = operator;
        this.left = new MathExpression(MathOperator.VALUE, left);
        this.left.setParent(this);

        this.right = new MathExpression(MathOperator.VALUE, right);
        this.right.setParent(this);
    }

    public Integer Evaluate() {
        switch (operator.getArity()) {
        case NULLARY:
            return value;
        case UNARY:
            return operator.Evaluate(left.Evaluate(), null);
        case BINARY:
            return operator.Evaluate(left.Evaluate(), right.Evaluate());

        }
        return operator.Evaluate(left.Evaluate(), right.Evaluate());
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj == this) {
            return true;
        }
        else if(obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        MathExpression other = (MathExpression) obj;
        if(this.operator != other.operator)
            return false;
        if(this.operator.hasValue() && this.value != other.value)
            return false;
        if(this.left != null) {
            if(other.left == null)
                return false;
            if(!this.left.equals(other.left))
                return false;
        }
        else if(other.left != null)
            return false;
        
        if(this.right != null) {
            if(other.right == null)
                return false;
            if(!this.right.equals(other.right))
                return false;
        }
        else if(other.right != null)
            return false;
        
        return true;
    }

    @Override
    public String toString() {
        if (operator.equals(MathOperator.NOOP)) {
            return "?";
        } else if (operator.getArity().equals(Arity.NULLARY)) {
            return value.toString();
        } else if (operator.getArity().equals(Arity.UNARY)) {
            if (left != null) {
                return left.toString() + operator.getOperatorChar();
            } else if (right != null) {
                return operator.getOperatorChar() + right.toString();
            }
        } else if (operator.getArity().equals(Arity.BINARY)) {
            boolean requireParentheses = hasParent();
            if (hasParent()) {
                if (parent.operator.getPredecence() < operator.getPredecence()) {
                    requireParentheses = true;
                }
            }
            if (requireParentheses) {
                return String.format("(%s%s%s)", left.toString(),
                        operator.getOperatorChar(), right.toString());
            } else {
                return left.toString() + operator.getOperatorChar()
                        + right.toString();
            }
        }

        return "WRONG ARITY";
    }
}
