package fi.utu.ville.exercises.mathpathexer.generator;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public class ExpressionGenerator {
    private static int MAX_GENERATION_ATTEMPTS = 100;
    Integer baseValue;
    Random random;
    ExpressionGeneratorParameters parameters;
    
    ArrayList<MathExpression> generatedExpressions = new ArrayList<MathExpression>();
    
    public ExpressionGenerator(ExpressionGeneratorParameters parameters) {
        this.parameters = parameters;
        this.random = new Random(parameters.getSeed());
        this.baseValue = random.nextInt(parameters.getMaxValue() - parameters.getMinValue()) + parameters.getMinValue();
    }
    
    public ExpressionGenerator() {
        this(new ExpressionGeneratorParameters());
    }
    
    public MathExpression generateRightExpression() {
        return generateUniqueExpression(true);
    }
    
    public MathExpression generateWrongExpression() {
        return generateUniqueExpression(false);
    }
    
    private MathExpression generateUniqueExpression(boolean trueExpression) {
        MathExpression exp = generate();
        for(int i = 1; i <= MAX_GENERATION_ATTEMPTS; i++) {
            if(!trueExpression) {
                falsifyExpression(exp);
            }
            if(!GeneratorHelper.existsInCollection(generatedExpressions, exp) || i==MAX_GENERATION_ATTEMPTS) {
                break;
            }
            exp = generate();
        }
        generatedExpressions.add(exp);
        return exp;
    }
    
    private void falsifyExpression(MathExpression exp) {
        MathExpression randomLeaf = randomLeafExpression(exp);
        Integer value = randomLeaf.getValue();

        Integer randomizer =0;
        while(randomizer == 0) {
            randomizer = random.nextInt(10) -5;
        }
        randomLeaf.setValue(value+randomizer);
    }

    private MathExpression generate() {
        MathExpression exp = new MathExpression(MathOperator.VALUE, baseValue);
        recursivelyGenerate(exp,1);
        return exp;
    }
    
    public ArrayList<MathExpression> generate(int count) {
        ArrayList<MathExpression> expressions = new ArrayList<MathExpression>();
        int attempts = 0;
        while(expressions.size() < count && attempts < MAX_GENERATION_ATTEMPTS) {
            MathExpression exp = generate();
            if(GeneratorHelper.existsInCollection(expressions, exp)) {
                attempts++;
                continue;
            }
            attempts=0;
            expressions.add(exp);
        }
        return expressions;
    }

    public void recursivelyGenerate(MathExpression exp, int curDepth) {
        double recursionEndProb = Math.pow(curDepth/(double)parameters.getMaxDepth(), 2);
        double r = random.nextDouble();
        if(r <= recursionEndProb) {
            return;
        }
        createMultiplicativeExpression(exp);
        MathOperator randomOperator = GeneratorHelper.randomFromSet(parameters.getOperators(), random);
        switch(randomOperator) {
        case MULTIPLICATION:
            createMultiplicativeExpression(exp);
            break;
        case DIVISION:
            createDivisionExpression(exp);
            break;
        case PLUS:
            createAdditiveExpression(exp);
            break;
        case MINUS:
            createNegationExpression(exp);
            break;
        }
        recursivelyGenerate(exp.getLeft(), curDepth+1);
        recursivelyGenerate(exp.getRight(), curDepth+1);
        
    }

    private void createAdditiveExpression(MathExpression exp) {
        Integer value = exp.getValue();
        int left = random.nextInt(value + 1);
        int right = value - left;
        exp.setExpression(MathOperator.PLUS, left, right);
    }

    private void createMultiplicativeExpression(MathExpression exp) {
        Integer value = exp.getValue();
        ArrayList<Integer> factors = factor(value);
        Collections.shuffle(factors, random);
        int left = 1, right = 1;
        if(factors.size() == 1) {
            factors.add(1);
        }
        if (factors.size() >= 2) {
            int splitIndex = random.nextInt(factors.size() - 1) + 1;
            for (int i = 0; i < splitIndex; i++) {
                left *= factors.get(i);
            }
            for (int i = splitIndex; i < factors.size(); i++) {
                right *= factors.get(i);
            }
        }
        exp.setExpression(MathOperator.MULTIPLICATION, left, right);
        
    }

    private void createNegationExpression(MathExpression exp) {
        Integer value = exp.getValue();
    }

    private void createDivisionExpression(MathExpression exp) {
        Integer value = exp.getValue();
        int maxMultiplier = parameters.getMaxValue();
        if(value != 0) {
            maxMultiplier /= value;
        }
        int multiplier = random.nextInt(maxMultiplier) + 1;
        int left = value * multiplier;
        int right = multiplier;
        exp.setExpression(MathOperator.DIVISION, left, right);
    }

    // Brute-force factorization algorithm
    private ArrayList<Integer> factor(int value) {
        ArrayList<Integer> factors = new ArrayList<Integer>();
        if(value==0) {
            factors.add(0);
            return factors;
        }
        int candidate = 2;
        while (value != 1) {
            if (value % candidate == 0) {
                factors.add(candidate);
                value /= candidate;
            } else {
                if (candidate != 2) {
                    candidate += 2;
                } else {
                    candidate += 1;
                }
            }
        }
        return factors;
    }
    
    private MathExpression randomLeafExpression(MathExpression exp) {
        if(exp.getLeft() == null) {
            if(exp.getRight() == null)
                return exp;
            else
                return randomLeafExpression(exp.getRight());
        }
        else if(exp.getRight() == null) {
            return randomLeafExpression(exp.getLeft());
        }
        else if(random.nextBoolean()) {
            return randomLeafExpression(exp.getLeft());
        }
        else {
            return randomLeafExpression(exp.getRight());
        }
            
    }
    
    public int getValue() {
        return baseValue;
    }

}
