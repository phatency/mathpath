package fi.utu.ville.exercises.mathpathexer.generator;

import java.util.Collection;
import java.util.Random;
import java.util.Set;

public class GeneratorHelper {
    
    public static <T> boolean existsInCollection(Collection<T> list, T value) {
        for(T element : list) {
            if(value.equals(element)) {
                return true;
            }
        }
        return false;
    }
    
    public static <T> T randomFromSet(Set<T> set, Random random) {
        int i = 0;
        int r = random.nextInt(set.size());
        for(T value : set) {
            if(i==r)
                return value;
            i++;
        }
        return null; // never happens
    }
}
