package fi.utu.ville.exercises.mathpathexer.generator;

import java.util.EnumSet;

public class ExpressionGeneratorParameters {
    long seed;
    int maxValue;
    int minValue;
    int maxDepth;
    EnumSet<MathOperator> operators;
    
    public ExpressionGeneratorParameters(long seed, int minValue, int maxValue, int maxDepth, EnumSet<MathOperator> operators) {
        this.seed = seed;
        this.minValue = minValue;
        this.maxValue = maxValue;
        this.maxDepth = maxDepth;
        this.operators = operators;
    }
    
    public ExpressionGeneratorParameters() {
        this(System.currentTimeMillis(), 0, 100, 3, MathOperator.ALL_OPERATORS);
    }
    
    public void setSeed(long seed) {
        this.seed = seed;
    }
    
    public long getSeed() {
        return seed;
    }
    
    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
    }
    
    public int getMaxValue() {
        return maxValue;
    }
    
    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }
    
    public int getMinValue() {
        return minValue;
    }
    
    public void setMaxDepth(int depth) {
        this.maxDepth = depth;  
    }
    
    public int getMaxDepth() {
        return maxDepth;
    }
    
    public void setOperators(EnumSet<MathOperator> operators) {
        this.operators = operators;
    }
    
    public EnumSet<MathOperator> getOperators() {
        return operators;
    }
    
    
}
